/* В папке calculator дана верстка макета калькулятора. Необходимо сделать этот калькулятор рабочим.
* При клике на клавиши с цифрами - набор введенных цифр должен быть показан на табло калькулятора.
* При клике на знаки операторов (`*`, `/`, `+`, `-`) на табло ничего не происходит - программа ждет введения второго числа для выполнения операции.
* Если пользователь ввел одно число, выбрал оператор, и ввел второе число, то при нажатии как кнопки `=`,
 так и любого из операторов, в табло должен появиться результат выполенния предыдущего выражения.
* При нажатии клавиш `M+` или `M-` в левой части табло необходимо показать маленькую букву `m` - это значит,
 что в памяти хранится число. Нажатие на `MRC` покажет число из памяти на экране.
  Повторное нажатие `MRC` должно очищать память. */

window.addEventListener("DOMContentLoaded", () => {
    const keys = document.querySelector(".keys"),                // переменная с дивом в котором находятся кнопки
        display = document.querySelector(".display input"),      // инпут дисплея
        divMrc = document.querySelector(".m")                   // див mrc

    let calculator = {                                           // объект калькулятора
        firstNumber: "",
        secondNumber: "",
        arithmeticSign: "",
        mrc: "",
        result: ""
    }

    let mrcCounter = 0;                                         // счетчик mrc

    keys.addEventListener("click", (e) => {
        validate(e.target.value);
    })



    const validate = (argument) => {
        if (/^[0-9.]$/.test(argument) && calculator.arithmeticSign == "") {                  // проверка первого числа
            if (calculator.result == "") {
                calculator.firstNumber += argument;
                mrcCounter = 0
            } else if (calculator.result !== "") {
                calculator.result = ""
                calculator.firstNumber = "";
                calculator.firstNumber += argument;
                mrcCounter = 0
            }
            if (/\.{2,}/.test(calculator.firstNumber)) {
                let replace = calculator.firstNumber.replace(/\.{2,}/g, ".");
                calculator.firstNumber = replace;
            } else if (/[0]{2,}\.+/.test(calculator.firstNumber)) {
                let replace0 = calculator.firstNumber.replace(/[0]{2,}\.+/g, "0.");
                calculator.firstNumber = replace0
            }

            console.log("первое число " + calculator.firstNumber);
            display.value = calculator.firstNumber
        } else if (calculator.firstNumber !== "" && calculator.secondNumber == "" && /^[+/*-]$/.test(argument)) { // проверка знака операции
            calculator.arithmeticSign = argument;
            console.log("знак " + calculator.arithmeticSign);
            mrcCounter = 0
        } else if (calculator.firstNumber !== "" && calculator.arithmeticSign !== "" && /^[0-9.]$/.test(argument)) { // проверка второго числа
            calculator.secondNumber += argument;
            display.value = calculator.secondNumber
            console.log("второе число " + calculator.secondNumber);
            mrcCounter = 0
            if (/\.{2,}/.test(calculator.secondNumber)) {
                let replace2 = calculator.secondNumber.replace(/\.{2,}/g, ".");
                calculator.secondNumber = replace2;
            } else if (/[0]{2,}\.+/.test(calculator.secondNumber)) {
                let replace02 = calculator.secondNumber.replace(/[0]{2,}\.+/g, "0.");
                calculator.secondNumber = replace02
            }
        } else if (calculator.secondNumber !== "" && calculator.arithmeticSign !== "" && /^[=+/*-]$/.test(argument)) { // результат
            if (calculator.arithmeticSign == "+") {
                calculator.result = +calculator.firstNumber + +calculator.secondNumber;
                calculator.firstNumber = calculator.result;
                calculator.secondNumber = "";
                calculator.arithmeticSign = "";
                display.value = calculator.result
                console.log(`Result - ${calculator.result}`);
                console.log(`firstnum - ${calculator.firstNumber}`);
            } else if (calculator.arithmeticSign == "-") {
                calculator.result = +calculator.firstNumber - +calculator.secondNumber;
                calculator.firstNumber = calculator.result;
                calculator.secondNumber = "";
                calculator.arithmeticSign = "";
                display.value = calculator.result
                console.log(`Result - ${calculator.result}`);
                console.log(`firstnum - ${calculator.firstNumber}`);
            } else if (calculator.arithmeticSign == "*") {
                calculator.result = +calculator.firstNumber * +calculator.secondNumber;
                calculator.firstNumber = calculator.result;
                calculator.secondNumber = "";
                calculator.arithmeticSign = "";
                display.value = calculator.result
                console.log(`Result - ${calculator.result}`);
                console.log(`firstnum - ${calculator.firstNumber}`);
            } else if (calculator.arithmeticSign == "/") {
                calculator.result = +calculator.firstNumber / +calculator.secondNumber;
                calculator.firstNumber = calculator.result;
                calculator.secondNumber = "";
                calculator.arithmeticSign = "";
                display.value = calculator.result
                console.log(`Result - ${calculator.result}`);
                console.log(`firstnum - ${calculator.firstNumber}`);
            }
            if (/^[+/*-]$/.test(argument)) {
                calculator.arithmeticSign = argument;
                console.log("знак присвоен")


            }


            mrcCounter = 0


        } else if (argument == "m-" || argument == "m+") {                                  // работаем с кнопками м+ и м-

            if (argument == "m-") {
                calculator.mrc = -display.value;
            } else if (argument == "m+") {
                calculator.mrc = +display.value;
            }
            console.log("mrc " + calculator.mrc);
            divMrc.innerHTML = "m"
        } else if (argument == "mrc") {                                                     // работаем с кнопкой mrc
            if (mrcCounter == 0) {
                if (calculator.secondNumber == "" && calculator.arithmeticSign == "" && calculator.mrc !== "") {
                    calculator.firstNumber = calculator.mrc;
                    console.log("mrc присвоили к firstNumber");
                    mrcCounter++
                } else if (calculator.firstNumber !== "" && calculator.arithmeticSign !== "" && calculator.mrc !== "") {
                    calculator.secondNumber = calculator.mrc;
                    console.log("mrc присвоили к secondNumber");
                    mrcCounter++
                }
            } else if (mrcCounter == 1) {
                calculator.mrc = "";
                divMrc.innerHTML = "";
                console.log(`mrc обнулилось`);
                mrcCounter = 0;
            }

        } else if (argument == "C") {                                    // кнопка С
            calculator.result = "";
            calculator.firstNumber = "";
            calculator.secondNumber = "";
            calculator.arithmeticSign = "";
            calculator.mrc = ""
            display.value = ""
            divMrc.innerHTML = ""
            console.log("Обнулялся")

        }
    }






})

