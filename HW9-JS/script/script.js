/* * Секундомер будет иметь 3 кнопки "Старт, Стоп, Сброс"<br>
            * При нажатии на кнопку стоп фон секундомера должен быть красным, старт - зеленый, сброс - серый
            * Вывод счётчиков в формате ЧЧ:ММ:СС<br>
            * Реализуйте Задание используя синтаксис ES6 и стрелочные функции */

window.addEventListener("DOMContentLoaded", () => {
    let display = document.querySelector(".stopwatch-display")   // обращаемся к дисплею
    let btns = document.querySelectorAll("button")    // обращаемся к кнопкм
    let startBtn = btns[0]
    let stopBtn = btns[1]
    let resetBtn = btns[2]

    let times = document.querySelectorAll("span")           // обращаемся к Времени 00 00 00 

    let secondsSpan = times[2]
    let minsSpan = times[1]
    let hoursSpan = times[0]

    let seconds = 0;
    let minutes = 0;
    let hours = 0;

    let counter = () => {                        // делаем счётчик 
        if (seconds == 60) {
            seconds = 0;
            minutes++;
        } else if (minutes == 60) {
            minutes = 0;
            hours++
        } else if (hours == 24) {
            hours = 0;;
        }
        if (seconds < 10) {                                       
            secondsSpan.innerHTML = "0" + seconds;                  // условная конструкция для формата  01 01 01
        } else if (seconds >= 10) {
            secondsSpan.innerHTML = seconds;
        }
        if (minutes < 10) {
            minsSpan.innerHTML = "0" + minutes;
        } else if (minutes >= 10) {
            minsSpan.innerHTML = minutes;
        }
        if (hours < 10) {
            hoursSpan.innerHTML = "0" + hours;
        } else if (hours >= 10) {
            hoursSpan.innerHTML = hours;
        }
        seconds++;
    }

    let start = () => {                                          //функция для кнопки старта
        timer = setInterval(counter, 1000);          
    }

    let stop = () => {                                   //функция для кнопки стопа
        clearInterval(timer)
    }

    let reset = () => {                                   //функция для кнопки ресета
        clearInterval(timer)
        seconds = 0;
        minutes = 0;
        hours = 0;
        secondsSpan.innerHTML = "0" + seconds;
        minsSpan.innerHTML = "0" + minutes;
        hoursSpan.innerHTML = "0" + hours;
    }

    startBtn.addEventListener("click", () => {                // кнопка старта
        display.style.backgroundColor = "green";
        start()
    })

    stopBtn.addEventListener("click", () => {             // кнопка стопа
        display.style.backgroundColor = "red";
        stop()
    })


    resetBtn.addEventListener("click", () => {               // кнопка ресета
        display.style.backgroundColor = "grey";
        reset()
    })
});
